Prestaciones SEPE, ayudas y subsidios para facilitar tus trámites.
Consejos laborales, que te proporcionarán un extra de motivación.
Noticias de trabajo, para mantenerte informado de los cambios que se produce en el ámbito laboral.
Autonómos, emprender y ser tu propio jefe con nuevos negocios.
Formación, los mejores cursos para no parar de aprender y seguir creciendo intelectualmente.

www.cocotrabajo.com